#include "buffer.hpp"

Buffer::Buffer(std::string name) {
    currentLine = lines.begin();
}

void Buffer::insertLineAtCursor(std::string line) {
    currentLine ++;
    lines.insert(currentLine, Rope(line));
    currentLine --;
}

void Buffer::appendLine(std::string line) {
    lines.push_back(Rope(line));
    currentLine = lines.end();
}

std::string Buffer::removeTabs(std::string str) {
    for (auto i = str.find('\t'); i != std::string::npos; i = str.find('\t', i))
        str.replace(i, 1, "    ");
    return str;
}
