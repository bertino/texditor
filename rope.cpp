#include "rope.hpp"

Rope::Rope() {
    str = new char[LEAF_LEN];
}

Rope::Rope(std::string a, Rope *parent, int l, int r){
    if (r == -1) r = a.length() - 1;
    left = right = nullptr;

    // We put half this in left subtree
    this -> parent = parent;

    // If string length is more
    if ((r-l) > LEAF_LEN) {
        str = nullptr;
        weight = (r - l)/2;
        int m = (l + r)/2;
        left = new Rope(a, this, l, m);
        right = new Rope(a, this, m+1, r);
    }
    else {
        weight = (r - l);
        int j = 0;
        str = new char[LEAF_LEN];
        for (int i=l; i <= r; i++)
            str[j++] = a[i];
    }
}


void Rope::UpdateWeights() {
    if (left) {
        left -> UpdateWeights();
        weight = left -> WeightSum();
    }
    if (right) {
        right -> UpdateWeights();
    }
}


int Rope::WeightSum() {
    if (str) return weight;

    int sum = 0;
    if (left) sum += left -> WeightSum();
    if (right) sum += right -> WeightSum();

    return sum;
}

std::string Rope::ReturnString() {
    std::string ret;
    if (!left && !right) ret += str + LEAF_LEN;
    if (left) ret += left -> ReturnString();
    if (right) ret += right -> ReturnString();
    return ret;
}


void Rope::PrintString() {
    if (!left && !right) std::cout << str << '-';
    if (left) left -> PrintString();
    if (right) right -> PrintString();
}


void Rope::PrintTree(unsigned int level, std::ostream& out){
    for (unsigned int i = 0; i < level; i++)
        out << "│   ";

    char frag[LEAF_LEN];
    std::strncpy(frag, str ? str : "<n>", LEAF_LEN);

    if (left)
        out << "├── " << frag << ". (" << weight << ")\n";
    else
        out << "└── " << frag << ". (" << weight << ")\n";

    if (right) right->PrintTree(level+1, out);

    if (left) left->PrintTree(level, out);
}


Rope Rope::operator+ (Rope &r) {
    Rope *tmp = new Rope();
    tmp -> parent = nullptr;
    tmp -> left = this;
    tmp -> right = &r;
    parent = r.parent = tmp;

    tmp -> weight = weight;

    tmp -> str = nullptr;

    return *tmp;
}

Rope Rope::operator+= (Rope &r) {
    return *this + r;
}
