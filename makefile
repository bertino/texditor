name = texditor

src = $(wildcard *.cpp)
obj = $(src:.cpp=.o)

LDFLAGS = -lncurses
CC = g++

$(name): $(obj)
	$(CC) -o $@ $^ $(LDFLAGS)

.PHONY: clean
clean:
	rm -f $(obj) $(name)
