#ifndef ROPE_H
#define ROPE_H

#include <iostream>
#include <cstring>
#include <string>

const int LEAF_LEN = 5;

class Rope{
public:
    Rope();
    Rope(std::string a, Rope *parent = nullptr, int l = 0, int r = -1);

    void UpdateWeights();
    int WeightSum();
    std::string ReturnString();
    void PrintString();
    void PrintTree(unsigned int level = 0, std::ostream& out = std::cout);

    Rope operator+ (Rope &r);
    Rope operator+= (Rope &r);

private:
    Rope *left, *right, *parent;
    char *str;
    int weight;
};

#endif /* ifndef ROPE_H */
