#ifndef BUFFER_H
#define BUFFER_H

#include <list>
#include <string>

#include "rope.hpp"

class Buffer{
public:
    Buffer(std::string name);

    std::pair<unsigned int, unsigned int> cursorPos;

    std::list<Rope> lines;
    std::list<Rope>::iterator currentLine;

    void insertLineAtCursor(std::string);
    void appendLine(std::string);
    void removeLine(int);

    static std::string removeTabs(std::string);

};

#endif /* ifndef BUFFER_H */
