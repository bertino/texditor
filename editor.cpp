#include "editor.hpp"


Editor::Editor() {
    filename = newFile();
    buffer = new Buffer(filename);
    initWindow();
}


Editor::Editor(std::string filename) {
    buffer = new Buffer(filename);
    initWindow();
}


Editor::Editor(int lines, int cols, int y, int x) {
    filename = newFile();
    buffer = new Buffer(filename);
    initWindow(lines, cols, y, x);
}


Editor::Editor(std::string filename, int lines, int cols, int y, int x) {
    buffer = new Buffer(filename);
    initWindow(lines, cols, y, x);
}


std::string Editor::newFile() {
    std::string filename;
    if (! fileExists("newfile.txt")) filename = "newfile.txt";
    else {
        unsigned int i = 1;
        filename = std::string("newfile") + std::to_string(i) + std::string(".txt");
        while (fileExists(filename) && i < UINT_MAX) {
            i++;
            filename = std::string("newfile") + std::to_string(i) + std::string(".txt");
        }
    }
    return filename;
}


void Editor::initWindow(int lines, int cols, int y, int x) {
    window = newwin(lines, cols, y, x);
    updateDisplayed();
}


void Editor::updateDisplayed() {
    firstDisplayed = buffer->lines.begin();
    for (lastDisplayed = buffer->lines.begin(); lastDisplayed != buffer->lines.end();) {

        
    }

}


void Editor::printBuffer() {
    int i = 0;
    for (auto currentLine : displayedLines) {
        mvwprintw (window, i, 0, currentLine.ReturnString().c_str());
        i++;
    }
    wclrtoeol(window);
}

void Editor::printStatusLine() {
    std::string tmp = "";
    switch (mode) {
        case 'n':
            tmp += "Normal";
            break;
        case 'v':
            tmp += "Visual";
            break;
        case 'i':
            tmp += "Insert";
            break;
    }

    tmp += " mode";
    mvwprintw (window, LINES-1, 0, tmp.c_str());

    tmp = "Line: " + std::to_string(buffer -> cursorPos.second) +
        "/" + std::to_string(buffer->lines.size());
    mvwprintw (window, LINES-1, COLS-1-tmp.length(), tmp.c_str());
}
