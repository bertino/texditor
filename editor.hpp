#ifndef EDITOR_H
#define EDITOR_H value

#include <ncurses.h>
#include <utility>
#include <string>
#include <list>
#include <climits>

#include "buffer.hpp"
#include "rope.hpp"

class Editor{
private:
    std::pair<unsigned int, unsigned int> cursorPos;
    std::list<std::string> displayedLines;
    std::list<std::string>::iterator firstDisplayed;
    std::list<std::string>::iterator lastDisplayed;

    std::string filename;
    char mode;

    WINDOW *window;
    Buffer *buffer;

    void moveUp();
    void moveDown();
    void moveRight();
    void moveLeft();

public:
    Editor();
    Editor(int lines, int cols, int y, int x);
    Editor(std::string);
    Editor(std::string, int lines, int cols, int y, int x);

    char getMode() {return mode;}

    static std::string newFile();
    void initWindow(int = LINES, int = COLS, int = 0, int = 0);
    void updateDisplayed();

    void handleInput(int);
    void printBuffer();
    void printStatusLine();

    static bool fileExists(std::string);
};

#endif /* ifndef EDITOR_H */
